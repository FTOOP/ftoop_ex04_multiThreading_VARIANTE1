package ch.ffhs.ftoop.p1.producerconsumer;

import java.util.concurrent.locks.ReentrantLock;

public class Speicher implements SpeicherIf {

	private int wert;
	private boolean hatWert = false;
	private ReentrantLock locker;
	
	/**
	 * Constructor
	 * @param wert Anfangswert
	 */
	public Speicher(int wert, ReentrantLock locker) {
		this.wert = wert;
		this.hatWert = true;
		this.locker = locker;
	}
	/**
	 * Constructor
	 * Ohne Parameter. wert = 0, neuer locker wird erstellt.
	 */
	public Speicher() {
		this.wert = 0;
		this.hatWert = true;
		this.locker = new ReentrantLock();
	}
	
	/**
	 * Gibt das ReentrantLock Objekt zureck
	 * @ return ReentrantLock locker
	 */
	public ReentrantLock getLocker(){
		return locker;
	}
	
	
	/**
	 * Gibt den aktuellen Wert zur�ck.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public synchronized int getWert() throws InterruptedException {
		return wert;
	}
	
	/**
	 * Setzt einen neuen aktuellen Wert.
	 * 
	 * @param wert
	 * @throws InterruptedException
	 */
	public synchronized void setWert(int wert) throws InterruptedException {
		this.wert = wert;
	}
	
	
	/**
	 * Gibt true zur�ck, wenn es einen neuen, noch nicht konsumierten Wert im
	 * Objekt hat.
	 * 
	 * @return
	 */
	public boolean isHatWert() {
		// TODO was bedeutet, dass es einen "nicht konsumierten Wert" hat?
		return hatWert;
	}

}
