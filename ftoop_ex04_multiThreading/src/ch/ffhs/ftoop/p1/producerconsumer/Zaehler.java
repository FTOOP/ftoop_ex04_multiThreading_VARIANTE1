package ch.ffhs.ftoop.p1.producerconsumer;

public class Zaehler extends Thread {

	private Speicher speicher;
	private int max, min;

	/**
	 * Constructor
	 * @param s
	 *            Das Speicherobject, das die aktuelle Zahl haelt.
	 * @param min
	 *            Der Startwert f�r den Zaehler
	 * @param max
	 *            Der Endwert f�r den Zaehler (einschliesslich)
	 */
	Zaehler(Speicher s, int min, int max) {
		this.speicher = s;
		this.max = max;
		this.min = min;
	}
	

	/**
	 * Diese Run Methode z�hlt den Wert in Speicher hoch - von min bis max (einschliesslich).
	 * Detail:
	 * Geloest ueber eine while Schleife mit jeweils dem Warteintervall zwischen jedem Schleifendurchlauf.
	 * Das Warteintervall wird ausgel�st durch die sleep() Methode, die von der Klasse Thread geerbt wurde.
	 * Mit einem Objekt der Klasse ReentrantLock, das ein Attribut des Speicher-Objekts ist, wird sichergestellt,
	 * dass Zahler und Drucker nicht gleichzeitig die Methode getWert() und setWert() ausfuehren. 
	 */
	@Override
	public void run() { 
		int counter = min;
		while(counter <= max){
			speicher.getLocker().lock();
			try {
				speicher.setWert(counter);
				counter++;
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				speicher.getLocker().unlock();
			}
			try {sleep(1);} catch (InterruptedException e) {e.printStackTrace();}
		}
	}

}
