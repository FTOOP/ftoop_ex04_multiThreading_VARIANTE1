package ch.ffhs.ftoop.p1.producerconsumer;

public class Drucker extends Thread {
	private Speicher speicher;
	private Zaehler zaehler;
	private int actualWert, lastWert;

	/**
	 * Constructor
	 * @param s
	 *            Das Speicherobject, das die aktuelle Zahl haelt.
	 * @param zaehler
	 * 			  Das Zaehlerobjekt, das die Zahl im Speicherobjekt schrittweise erhoeht.
	 */
	Drucker(Speicher s, Zaehler zaehler) {
		this.speicher = s;
		this.zaehler = zaehler;
		lastWert = -Integer.MAX_VALUE;
	}

	/**
	 * Holt einen Wert vom Zaehler und gibt ihn aus, gefolgt von einem einzelnen Leerzeichen.
	 * Detail:
	 * Die Methode greift auf die Zahler-Instanz zu, die im Konstruktor uebergeben wurde.
	 * Mittels isAlive() wird geprueft, ob der Thread noch laeuft. Solange er laeuft, wird eine while Schleife durchlaufen.
	 * Innerhalb der while Schleife wird auf die Speicher-Instanz zugegriffen und der Wert ausgegeben, falls er sich vom letzten Wert unterscheidet.
	 * Mit einem Objekt der Klasse ReentrantLock, das ein Attribut des Speicher-Objekts ist, wird sichergestellt,
	 * dass Zahler und Drucker nicht gleichzeitig die Methode getWert() und setWert() ausfuehren.
	 */
	@Override
	public void run() {
		while (zaehler.isAlive()) {
			if(speicher.isHatWert()){
				speicher.getLocker().lock();
				try {
					actualWert = speicher.getWert();
					if(actualWert!=lastWert) {
						System.out.print(actualWert + " ");
					}
					lastWert = actualWert;
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					speicher.getLocker().unlock();
				}
			}
		}

	}

}
