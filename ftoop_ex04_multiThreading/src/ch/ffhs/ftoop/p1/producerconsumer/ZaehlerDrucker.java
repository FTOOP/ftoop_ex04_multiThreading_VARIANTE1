package ch.ffhs.ftoop.p1.producerconsumer;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * Der Aufruf ben�tigt zwei Parameter min und max - der Zaehler beginnt bei min
 * zu zaehlen und terminiert bei max.
 * 
 */

public class ZaehlerDrucker {

	public static void main(String[] args) throws InterruptedException {
		if (args.length != 2) {
			System.out.println("Usage: ZaehlerDrucker <min> <max>");
			System.exit(1);
		}
		
		// Alle noetigen Objekte erzeugen.
		Speicher s = new Speicher(Integer.parseInt(args[0]), new ReentrantLock());
		Zaehler z = new Zaehler(s, Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		Drucker d = new Drucker(s, z);
		
		// Erst zaehler thread starten, dann 50 ms warten, dann drucker thread starten.
		z.start();
		d.start();

		// Genuegend lange warten, bis die threads durchlaufen sind.
		z.join();
		d.join();

	}

}
