package ch.ffhs.ftoop.p1.producerconsumer;

/**
 * 
 * Der Aufruf ben�tigt zwei Parameter min und max - der Zaehler beginnt bei min
 * zu zaehlen und terminiert bei max.
 * 
 */

public class ZaehlerDrucker {

	public static void main(String[] args) throws InterruptedException {
		if (args.length != 2) {
			System.out.println("Usage: ZaehlerDrucker <min> <max>");
			System.exit(1);
		}
		
		// Alle noetigen Objekte erzeugen.
		Speicher s = new Speicher();
		Zaehler z = new Zaehler(s, Integer.parseInt(args[0]), Integer.parseInt(args[1]), 100);
		Drucker d = new Drucker(s, z, z.getWaitMs());
		
		// Erst zaehler thread starten, dann 50 ms warten, dann drucker thread starten.
		z.start();
		try{
			Thread.sleep(50);
		} catch (InterruptedException e){
			e.printStackTrace();
		}
		d.start();

		// Genuegend lange warten, bis die threads durchlaufen sind.
		Thread.sleep( Math.abs( Integer.parseInt(args[1])-Integer.parseInt(args[0]) )*100 + 100 );

	}

}
