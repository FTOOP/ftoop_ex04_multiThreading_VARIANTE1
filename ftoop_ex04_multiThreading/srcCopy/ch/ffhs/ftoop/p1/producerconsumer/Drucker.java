package ch.ffhs.ftoop.p1.producerconsumer;

public class Drucker extends Thread {
	private Speicher speicher;
	private int waitMs;
	private Zaehler zaehler;

	/**
	 * Constructor
	 * @param s
	 *            Das Speicherobject, das die aktuelle Zahl haelt.
	 * @param zaehler
	 * 			  Das Zaehlerobjekt, das die Zahl im Speicherobjekt schrittweise erhoeht.
	 * @param waitMs
	 *            Die Anzahl Millisekunden, die gewartet werden soll, bevor der naechste Wert ausgelesen wird.
	 */
	Drucker(Speicher s, Zaehler zaehler, int waitMs) {
		this.speicher = s;
		this.waitMs = waitMs;
		this.zaehler = zaehler;
	}

	/**
	 * Holt einen Wert vom Zaehler und gibt ihn aus, gefolgt von einem einzelnen Leerzeichen.
	 * Detail:
	 * Die Methode greift auf die Zahler-Instanz zu, die im Konstruktor uebergeben wurde.
	 * Mittels isAlive() wird geprueft, ob der Thread noch laeuft. Solange er laeuft, wird eine while Schleife durchlaufen.
	 * Innerhalb der while Schleife wird auf die Speicher-Instanz zugegriffen und der Wert ausgelesen.
	 */
	@Override
	public void run() {
		while (zaehler.isAlive()) {
			try {
				if(speicher.isHatWert()){
					System.out.print(speicher.getWert() + " ");
				}
				sleep(waitMs);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
